package br.com.decora.dao;

import br.com.decora.entity.User;
import br.com.decora.util.Base64;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.Objects;

/**
 * @author Daniel Queiroz
 */
public class UserDAO extends BasicDAO<User, ObjectId> {

    public UserDAO(final Datastore ds) {
        super(ds);
        this.ensureIndexes();
    }

    public User findByLogin(String login) {
        return createQuery().field("login").equal(login).get();
    }

    public boolean compareOldPassword(String login, String oldPassword) {
        User user = createQuery().field("login").equal(login).field("password").equal(Base64.encrypt(oldPassword)).get();
        return Objects.nonNull(user);
    }

    public void updatePassword(String login, String newPassword) {
        Query<User> query = createQuery().field("login").equal(login);
        UpdateOperations<User> ops = createUpdateOperations().set("password", Base64.encrypt(newPassword));
        update(query, ops);
    }
}
