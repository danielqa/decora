package br.com.decora.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;

import javax.validation.constraints.Size;

/**
 * @author Daniel Queiroz
 */
@Entity("users")
public class User extends BaseEntity {

    @Indexed(unique = true)
    @NotEmpty(message = "CAMPO_OBRIGATORIO")
    @Size(max = 50, message = "TAMANHO_MAXIMO")
    private String login;

    @NotEmpty(message = "CAMPO_OBRIGATORIO")
    private String password;

    @NotEmpty(message = "CAMPO_OBRIGATORIO")
    @Size(max = 100, message = "TAMANHO_MAXIMO")
    private String name;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
