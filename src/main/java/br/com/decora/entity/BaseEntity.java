package br.com.decora.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Transient;

import java.util.Date;
import java.util.Objects;

/**
 * @author Daniel Queiroz
 */
public class BaseEntity {

    @Id
    @JsonIgnore
    protected ObjectId id;

    protected Date creationDate;
    protected Date lastChange;

    @Transient
    protected String hash;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getLastChange() {
        return lastChange;
    }

    public String getHash() {
        return Objects.nonNull(id) ? id.toString() : hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @PrePersist
    public void prePersist() {
        this.creationDate = Objects.isNull(creationDate) ? new Date() : creationDate;
        this.lastChange = Objects.isNull(lastChange) ? creationDate : new Date();
    }
}
