package br.com.decora.business;

import br.com.decora.dao.UserDAO;
import br.com.decora.dto.ChangePasswordDTO;
import br.com.decora.entity.User;
import br.com.decora.enumeration.Messages;
import br.com.decora.exception.ResultException;
import br.com.decora.resource.UserResource;
import br.com.decora.util.Base64;
import br.com.decora.util.DatastoreFactory;
import br.com.decora.util.Result;
import org.apache.shiro.SecurityUtils;
import org.bson.types.ObjectId;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

/**
 * @author Daniel Queiroz
 */
@Stateless
public class UserBusiness implements UserResource {

    @Inject
    private Result result;

    private UserDAO dao;

    public UserBusiness() {
    }

    @Inject
    public UserBusiness(DatastoreFactory factory) {
        this.dao = new UserDAO(factory.getInstance());
    }

    public void save(User user) {
        beforeSave(user);
        dao.save(user);
        result.addMessage(Messages.GRAVADO_SUCESSO);
    }

    public void delete(String hash) {
        dao.deleteById(new ObjectId(hash));
        result.addMessage(Messages.REMOVIDO_SUCESSO);
    }

    public User get(String hash) {
        return dao.get(new ObjectId(hash));
    }

    public List<User> find() {
        return dao.find().asList();
    }

    public void updatePassword(ChangePasswordDTO dto) {
        String login = (String) SecurityUtils.getSubject().getPrincipal();
        String oldPassword = dto.getOldPassword();
        String newPassword = dto.getNewPassword();
        String confirmNewPassword = dto.getConfirmNewPassword();

        validateChangePassword(login, oldPassword, newPassword, confirmNewPassword);
        dao.updatePassword(login, newPassword);
        result.addMessage(Messages.SENHA_ALTERADA_SUCESSO);
    }

    private void beforeSave(User user) {
        user.setPassword(Base64.encrypt(user.getPassword()));
    }

    private void validateChangePassword(String login, String oldPassword, String newPassword, String confirmNewPassword) {
        if (Objects.isNull(oldPassword) || oldPassword.isEmpty()) {
            throw new ResultException(Messages.SENHA_ANTIGA_OBRIGATORIO);
        }
        if (Objects.isNull(newPassword) || newPassword.isEmpty()) {
            throw new ResultException(Messages.SENHA_NOVA_OBRIGATORIO);
        }
        if (Objects.isNull(confirmNewPassword) || confirmNewPassword.isEmpty()) {
            throw new ResultException(Messages.SENHA_NOVA_CONFIRMACAO_OBRIGATORIO);
        }
        if (!dao.compareOldPassword(login, oldPassword)) {
            throw new ResultException(Messages.SENHA_ANTIGA_INVALIDA);
        }
        if (!newPassword.equals(confirmNewPassword)) {
            throw new ResultException(Messages.SENHA_NOVA_CONFIRMACAO_INVALIDA);
        }
        if (newPassword.equals(oldPassword)) {
            throw new ResultException(Messages.SENHA_NOVA_IGUAL_SENHA_ANTIGA);
        }
    }
}
