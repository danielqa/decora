package br.com.decora.business;

import br.com.decora.dao.UserDAO;
import br.com.decora.entity.User;
import br.com.decora.enumeration.Messages;
import br.com.decora.exception.ResultException;
import br.com.decora.resource.LoginResource;
import br.com.decora.resource.UserResource;
import br.com.decora.util.Base64;
import br.com.decora.util.DatastoreFactory;
import br.com.decora.util.Result;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Objects;

/**
 * @author Daniel Queiroz
 */
@Stateless
public class LoginBusiness implements LoginResource {

    @EJB
    private UserResource userResource;

    @Inject
    private Result result;

    private UserDAO userDAO;

    public LoginBusiness() {
    }

    @Inject
    public LoginBusiness(DatastoreFactory factory) {
        this.userDAO = new UserDAO(factory.getInstance());
    }

    public void login(User user) {
        validateLogin(user);
        UsernamePasswordToken token = new UsernamePasswordToken(user.getLogin(), Base64.encrypt(user.getPassword()));
        initialUser(user);
        SecurityUtils.getSubject().login(token);
        sessionUser(user.getLogin());
    }

    public void logout() {
        SecurityUtils.getSubject().logout();
        result.addMessage(Messages.LOGOUT);
    }

    public void isAuthenticated() {
        boolean isAuthenticated = SecurityUtils.getSubject().isAuthenticated();
        result.include("isAuthenticated", isAuthenticated);
    }

    public void findSessionUser() {
        if (Objects.nonNull(SecurityUtils.getSubject()) && SecurityUtils.getSubject().isAuthenticated()) {
            includeSessionUser();
        }
    }

    private void validateLogin(User user) {
        if (Objects.isNull(user.getLogin()) || user.getLogin().isEmpty()) {
            throw new ResultException(Messages.LOGIN_OBRIGATORIO);
        }
        if (Objects.isNull(user.getPassword()) || user.getPassword().isEmpty()) {
            throw new ResultException(Messages.SENHA_OBRIGATORIO);
        }
    }

    private void initialUser(User user) {
        String initialLogin = "admin";
        if (initialLogin.equals(user.getLogin()) && Long.valueOf(userDAO.count()).equals(0L)) {
            user.setName("Administrador");
            userResource.save(user);
        }
    }

    private void sessionUser(String login) {
        User user = userDAO.findByLogin(login);
        SecurityUtils.getSubject().getSession().setAttribute("username", user.getName());
        includeSessionUser();
    }

    private void includeSessionUser() {
        result.include("login", SecurityUtils.getSubject().getPrincipal());
        result.include("username", SecurityUtils.getSubject().getSession().getAttribute("username"));
    }
}
