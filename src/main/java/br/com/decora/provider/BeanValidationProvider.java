package br.com.decora.provider;

import br.com.decora.enumeration.Messages;
import br.com.decora.util.Result;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Queiroz
 */
@Provider
public class BeanValidationProvider implements ExceptionMapper<ConstraintViolationException> {

    @Inject
    private Result result;

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        result.addMessage(Messages.ERRO_VALIDACAO);

        for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
            String property = violation.getPropertyPath().toString();
            Messages message = Messages.valueOf(violation.getMessage());

            if (Messages.containsKey(property)) {
                result.addMessage(message, Messages.valueOf(property).toString());
            } else {
                result.addMessage(message, StringUtils.capitalize(property));
            }
        }

        return result.asResponse(Status.PRECONDITION_FAILED);
    }

}