package br.com.decora.provider;

import br.com.decora.enumeration.Messages;
import br.com.decora.util.Result;
import org.apache.shiro.authc.AuthenticationException;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Queiroz
 */
@Provider
public class AuthenticationProvider implements ExceptionMapper<AuthenticationException> {

    @Inject
    private Result result;

    @Override
    public Response toResponse(AuthenticationException exception) {
        return result.addMessage(Messages.LOGIN_SENHA_INVALIDO).asResponse(Status.NOT_ACCEPTABLE);
    }

}
