package br.com.decora.provider;

import br.com.decora.exception.ResultException;
import br.com.decora.util.Result;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Queiroz
 */
@Provider
public class ResultProvider implements ExceptionMapper<ResultException> {

    @Inject
    private Result result;

    @Override
    public Response toResponse(ResultException exception) {
        return result.addMessage(exception.getMessage()).asResponse(Status.PRECONDITION_FAILED);
    }
}
