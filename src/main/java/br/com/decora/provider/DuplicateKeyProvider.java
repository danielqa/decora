package br.com.decora.provider;

import br.com.decora.enumeration.Messages;
import br.com.decora.util.Result;
import com.mongodb.DuplicateKeyException;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Queiroz
 */
@Provider
public class DuplicateKeyProvider implements ExceptionMapper<DuplicateKeyException> {

    @Inject
    private Result result;

    @Override
    public Response toResponse(DuplicateKeyException exception) {
        return result.addMessage(Messages.ERRO_VALIDACAO).addMessage(Messages.UNIQUE_KEY).asResponse(Status.PRECONDITION_FAILED);
    }

}