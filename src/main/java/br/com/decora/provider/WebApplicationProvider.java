package br.com.decora.provider;

import br.com.decora.enumeration.Messages;
import br.com.decora.util.Result;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Daniel Queiroz
 */
@Provider
public class WebApplicationProvider implements ExceptionMapper<WebApplicationException> {

    @Inject
    private Result result;

    @Override
    public Response toResponse(WebApplicationException exception) {
        LoggerFactory.getLogger(exception.getClass()).error("", exception);
        return result.addMessage(Messages.ERRO_INESPERADO).asResponse(Status.INTERNAL_SERVER_ERROR);
    }

}
