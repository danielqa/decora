package br.com.decora.enumeration;

import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * @author Daniel Queiroz
 */
public enum Messages {

    /**
     * Messages
     */
    GRAVADO_SUCESSO,
    REMOVIDO_SUCESSO,
    SENHA_ALTERADA_SUCESSO,
    CAMPO_OBRIGATORIO,
    TAMANHO_MAXIMO,
    LOGOUT,
    UNIQUE_KEY,
    LOGIN_SENHA_INVALIDO,
    LOGIN_OBRIGATORIO,
    SENHA_OBRIGATORIO,
    SENHA_ANTIGA_OBRIGATORIO,
    SENHA_ANTIGA_INVALIDA,
    SENHA_NOVA_OBRIGATORIO,
    SENHA_NOVA_CONFIRMACAO_OBRIGATORIO,
    SENHA_NOVA_CONFIRMACAO_INVALIDA,
    SENHA_NOVA_IGUAL_SENHA_ANTIGA,
    ERRO_INESPERADO,
    ERRO_VALIDACAO,

    /**
     * Labels
     */
    password,
    name;

    private String message;

    Messages() {
        try {
            String message_ = ResourceBundle.getBundle("messages").getString(name());
            this.message = new String(message_.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LoggerFactory.getLogger(Messages.class).error("", e);
        }
    }

    public static boolean containsKey(String property) {
        return ResourceBundle.getBundle("messages").containsKey(property);
    }

    public String format(Object... arguments) {
        return MessageFormat.format(this.message, arguments);
    }

    @Override
    public String toString() {
        return message;
    }
}
