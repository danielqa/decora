package br.com.decora.util;

import org.apache.shiro.crypto.hash.Sha256Hash;

import java.util.Objects;

/**
 * @author Daniel Queiroz
 */
public class Base64 {

    public static String encrypt(Object source) {
        if (Objects.nonNull(source) && !source.toString().isEmpty()) {
            return new Sha256Hash(source).toBase64();
        }
        return null;
    }
}
