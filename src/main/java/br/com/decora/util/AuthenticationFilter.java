package br.com.decora.util;

import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Daniel Queiroz
 */
public class AuthenticationFilter extends BasicHttpAuthenticationFilter {

    @Override
    protected boolean sendChallenge(ServletRequest request, ServletResponse response) {
        LoggerFactory.getLogger(AuthenticationFilter.class).info("Authentication required: sending 401 Authentication challenge response.");
        HttpServletResponse httpResponse = WebUtils.toHttp(response);
        httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        return false;
    }
}