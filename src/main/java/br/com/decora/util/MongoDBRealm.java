package br.com.decora.util;

import com.mongodb.*;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * @author Daniel Queiroz
 */
public class MongoDBRealm extends AuthorizingRealm {

    protected DBCollection collection;

    public MongoDBRealm() {
        Mongo mongo = new Mongo();
        DB db = mongo.getDB("decoraDB");
        this.collection = db.getCollection("users");
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken) {
        if (!(authToken instanceof UsernamePasswordToken)) {
            throw new AuthenticationException("This realm only supports UsernamePasswordTokens");
        }

        UsernamePasswordToken token = (UsernamePasswordToken) authToken;

        if (token.getUsername() == null) {
            throw new AuthenticationException("Cannot log in null user");
        }

        return findPasswordForUsername(token.getUsername());
    }

    private AuthenticationInfo findPasswordForUsername(String username) {
        DBObject obj = collection.findOne(new BasicDBObject("login", username));

        if (obj == null) {
            throw new UnknownAccountException("Unknown user " + username);
        }

        String password = obj.get("password").toString();
        return new SimpleAuthenticationInfo(username, password.toCharArray(), getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }
}
