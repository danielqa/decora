package br.com.decora.util;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.ValidationExtension;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Daniel Queiroz
 */
@ApplicationScoped
public class DatastoreFactory {

    private final Datastore ds;

    public DatastoreFactory() {
        final Morphia morphia = new Morphia();
        morphia.mapPackage("br.com.decora");
        new ValidationExtension(morphia);
        this.ds = morphia.createDatastore(new MongoClient(), "decoraDB");
    }

    public Datastore getInstance() {
        return ds;
    }
}
