package br.com.decora.util;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Daniel Queiroz
 */
@ApplicationPath("/rest")
public class JaxRsActivator extends Application {
}