package br.com.decora.resource;

import br.com.decora.dto.ChangePasswordDTO;
import br.com.decora.entity.User;

import javax.ws.rs.*;
import java.util.List;

/**
 * @author Daniel Queiroz
 */
@Path("/user")
public interface UserResource {

    @POST
    void save(User user);

    @DELETE
    @Path("/{hash}")
    void delete(@PathParam("hash") String hash);

    @GET
    @Path("/{hash}")
    User get(@PathParam("hash") String hash);

    @GET
    List<User> find();

    @PUT
    @Path("/update-password")
    void updatePassword(ChangePasswordDTO dto);
}
