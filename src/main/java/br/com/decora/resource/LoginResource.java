package br.com.decora.resource;

import br.com.decora.entity.User;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * @author Daniel Queiroz
 */
@Path("/security")
public interface LoginResource {

    @POST
    @Path("/login")
    void login(User user);

    @POST
    @Path("/logout")
    void logout();

    @GET
    @Path("/authenticated")
    void isAuthenticated();

    @GET
    @Path("/find-session-user")
    void findSessionUser();
}
