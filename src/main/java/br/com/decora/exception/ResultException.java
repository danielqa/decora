package br.com.decora.exception;

import br.com.decora.enumeration.Messages;

import javax.ejb.EJBException;

/**
 * @author Daniel Queiroz
 */
public class ResultException extends EJBException {

    public ResultException(String message) {
        super(message);
    }

    public ResultException(Messages messages) {
        this(messages.toString());
    }
}
