# decora

Projeto teste Server.


## Install

### Baixe e descompacte o servidor de aplicação Wildfly

```sh
$ wget http://download.jboss.org/wildfly/9.0.2.Final/wildfly-9.0.2.Final.tar.gz
$ tar -xzvf wildfly-9.0.2.Final.tar.gz
```

### Instale o GIT (caso não tenha instalado)

```sh
$ sudo apt-get install git
```

### Clone o projeto decora (server)

```sh
$ git clone https://bitbucket.org/danielqa/decora.git
```

### Instale o Java 8

```sh
$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java8-installer
```

### Instale o MongoDB

```sh
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
$ echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
$ sudo apt-get update
$ sudo apt-get install mongodb
```

### Instale o Gradle

```sh
$ sudo add-apt-repository ppa:cwchien/gradle
$ sudo apt-get update
$ sudo apt-get install gradle
```

### Gere a build do projeto

```sh
$ cd decora
$ gradle war
```

### Copie o war para o Wildfly

```sh
$ cd ..
$ cp decora/build/libs/decora-1.0.0.war wildfly-9.0.2.Final/standalone/deployments/
```

### Suba o servidor de aplicação Wildfly (back-ground)

```sh
$ sh wildfly-9.0.2.Final/bin/standalone.sh &
```

